package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project internal = new Project(ProjectType.INTERNAL, "internal");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(internal);
        // then
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE,expenseCodeByProject);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project ProjectA = new Project(ProjectType.EXTERNAL, "Project A");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(ProjectA);
        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_A,expenseCodeByProject);
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project projectB = new Project(ProjectType.EXTERNAL, "Project B");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(projectB);
        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_B,expenseCodeByProject);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project other = new Project(ProjectType.EXTERNAL, "other");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(other);
        // then
        assertEquals(ExpenseType.OTHER_EXPENSE,expenseCodeByProject);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project unexpected = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "unexpected");
        ExpenseService expenseService = new ExpenseService();
        // when
        Executable executable = () -> expenseService.getExpenseCodeByProject(unexpected);
        // then
        assertThrows(UnexpectedProjectTypeException.class,executable);
    }
}